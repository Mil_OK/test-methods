$(document).ready(function(){
    var table;

    $("#add_new_customer").click(function() {
        $.redirect('/endpoint/add_customer.html', {'login': 'admin', 'pass': 'setup', 'role': 'ADMIN'}, 'GET');
    });
    $("#plans").click(function() {
        $.redirect('/endpoint/plans.html', {'login': 'admin', 'pass': 'setup', 'role': 'ADMIN'}, 'GET');
    });
    $('#edit_customer').click( function () {
        if (table.row('.selected').data() != null) {
            sessionStorage.setItem('first_name', table.row('.selected').data()[0]);
            sessionStorage.setItem('last_name', table.row('.selected').data()[1]);
            sessionStorage.setItem('email', table.row('.selected').data()[2]);
            sessionStorage.setItem('password', table.row('.selected').data()[3]);
            $.redirect('/endpoint/edit_customer.html', {'login': 'admin', 'pass': 'setup', 'role': 'ADMIN'}, 'GET');
        }
    } );
    $('#delete_customer').click( function () {
        if (table.row('.selected').data() != null) {
            $.get({
                url: 'rest/get_customer_id/' + table.row('.selected').data()[2],
                headers: {
                    'Authorization': 'Basic ' + btoa('admin' + ':' + 'setup'),
                    'Content-Type': 'application/json'
                }
            }).done(function(data) {
                $.ajax({
                    type: "DELETE",
                    url: "rest/customer?id=" + data.substr(1, data.length - 2),
                    headers: {
                        'Authorization': 'Basic ' + btoa('admin' + ':' + 'setup'),
                        'Content-Type': 'application/json'
                    },
                    success: function(msg){
                        table.row('.selected').remove().draw( false );
                    }
                });
            });
        }
    } );

    $.get({
        url: 'rest/customer',
        headers: {
            'Authorization': 'Basic ' + btoa('admin' + ':' + 'setup')
        }
    }).done(function(data) {
        var json = $.parseJSON(data);

        var dataSet = [];
        for(var i = 0; i < json.length; i++) {
            var obj = json[i];
            dataSet.push([obj.firstName, obj.lastName, obj.login, obj.pass, obj.balance])
        }

        //$("#customer_list_id").html(data);
        table = $('#customer_list_id')
            .DataTable({
                data: dataSet,
                columns: [
                    { title: "Fist Name" },
                    { title: "Last Name" },
                    { title: "Email" },
                    { title: "Pass" },
                    { title: "Balance" }
                ]
            });
        $('#customer_list_id tbody').on( 'click', 'tr', function () {
            if ( $(this).hasClass('selected') ) {
                $(this).removeClass('selected');
            }
            else {
                table.$('tr.selected').removeClass('selected');
                $(this).addClass('selected');
            }
        } );
    });
});
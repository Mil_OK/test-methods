$(document).ready(function(){
    var table;

    $("#add_new_plan").click(function() {
        $.redirect('/endpoint/add_plan.html', {'login': 'admin', 'pass': 'setup', 'role': 'ADMIN'}, 'GET');
    });
    $("#customers").click(function() {
        $.redirect('/endpoint/customers.html', {'login': 'admin', 'pass': 'setup', 'role': 'ADMIN'}, 'GET');
    });
    $('#edit_plan').click( function () {
        if (table.row('.selected').data() != null) {
            sessionStorage.setItem('name', table.row('.selected').data()[0]);
            sessionStorage.setItem('details', table.row('.selected').data()[1]);
            sessionStorage.setItem('fee', table.row('.selected').data()[2]);
            $.redirect('/endpoint/edit_plan.html', {'login': 'admin', 'pass': 'setup', 'role': 'ADMIN'}, 'GET');
        }
    });
    $('#delete_plan').click( function () {
        if (table.row('.selected').data() != null) {
            $.get({
                url: 'rest/get_plan_id/' + table.row('.selected').data()[0],
                headers: {
                    'Authorization': 'Basic ' + btoa('admin' + ':' + 'setup'),
                    'Content-Type': 'application/json'
                }
            }).done(function(data) {
                $.ajax({
                    type: "DELETE",
                    url: "rest/plan?id=" + data.substr(1, data.length - 2),
                    headers: {
                        'Authorization': 'Basic ' + btoa('admin' + ':' + 'setup'),
                        'Content-Type': 'application/json'
                    },
                    success: function(msg){
                        table.row('.selected').remove().draw( false );
                    }
                });
            });
        }
    });

    $.get({
        url: 'rest/plan',
        headers: {
            'Authorization': 'Basic ' + btoa('admin' + ':' + 'setup')
        }
    }).done(function(data) {
        var json = $.parseJSON(data);

        var dataSet = [];
        for(var i = 0; i < json.length; i++) {
            var obj = json[i];
            dataSet.push([obj.name, obj.details, obj.fee])
        }

        //$("#plan_list_id").html(data);
        table = $('#plan_list_id')
            .DataTable({
                data: dataSet,
                columns: [
                    { title: "Name" },
                    { title: "Details" },
                    { title: "Fee" }
                ]
            });
        $('#plan_list_id tbody').on( 'click', 'tr', function () {
            if ( $(this).hasClass('selected') ) {
                $(this).removeClass('selected');
            }
            else {
                table.$('tr.selected').removeClass('selected');
                $(this).addClass('selected');
            }
        } );
    });
});
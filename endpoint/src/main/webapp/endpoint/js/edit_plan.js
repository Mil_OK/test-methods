$(document).ready(function(){
    $("#name_id").val(sessionStorage.getItem('name'));
    $("#details_id").val(sessionStorage.getItem('details'));
    $("#fee_id").val(sessionStorage.getItem('fee'));

    $("#edit_plan_id").click(
        function() {
            var name = $("#name_id").val();
            var details = $("#details_id").val();
            var fee = $("#fee_id").val();

            // check fields
            if(name =='' || fee =='') {
                $('input[type="text"],input[type="number"]').css("border","2px solid red");
                $('input[type="text"],input[type="number"]').css("box-shadow","0 0 3px red");
                alert("Name or fee is empty");
            } else {
                $.ajax({
                    type: "PUT",
                    url: "rest/plan",
                    headers: {
                        'Authorization': 'Basic ' + btoa('admin' + ':' + 'setup'),
                        'Content-Type': 'application/json'
                    },
                    data: JSON.stringify({
                        "name":name,
                        "details":details,
                        "fee":fee
                    }),
                    success: function(msg){
                        $.redirect('/endpoint/plans.html', {'login': 'admin', 'pass': 'setup', 'role': 'ADMIN'}, 'GET');
                    }
                });
            }
        }
    );
});
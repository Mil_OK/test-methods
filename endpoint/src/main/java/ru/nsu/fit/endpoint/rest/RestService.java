package ru.nsu.fit.endpoint.rest;

import org.apache.commons.lang.Validate;
import org.apache.commons.lang.exception.ExceptionUtils;
import org.slf4j.Logger;
import ru.nsu.fit.endpoint.service.MainFactory;
import ru.nsu.fit.endpoint.service.database.data.Customer;
import ru.nsu.fit.endpoint.service.database.data.Plan;
import ru.nsu.fit.endpoint.service.database.data.Subscription;
import ru.nsu.fit.endpoint.shared.JsonMapper;

import javax.annotation.security.RolesAllowed;
import javax.ws.rs.*;
import javax.ws.rs.container.ContainerRequestContext;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import java.util.List;
import java.util.UUID;

@Path("")
@Consumes(MediaType.APPLICATION_JSON)
public class RestService {
    private static final Logger L = MainFactory.getInstance().getLogger();
    private static final String CUSTOMER_PATH = "/customer";
    private static final String PLAN_PATH = "/plan";
    private static final String SUBSCRIPTION_PATH = "/subscription";

    @RolesAllowed({AuthenticationFilter.UNKNOWN, AuthenticationFilter.ADMIN})
    @GET
    @Path("/health_check")
    public Response healthCheck() {
        return Response.ok().entity("{\"status\": \"OK\"}").build();
    }

    @RolesAllowed({AuthenticationFilter.UNKNOWN , AuthenticationFilter.ADMIN})
    @GET
    @Path("/get_role")
    public Response getRole(@Context ContainerRequestContext crc) {
        return Response.ok().entity(String.format("{\"role\": \"%s\"}", crc.getProperty("ROLE"))).build();
    }

    @RolesAllowed(AuthenticationFilter.UNKNOWN)
    @POST
    @Path("update_customer_profile/{customer_login}")
    public Response updateCustomerProfile(@PathParam("customer_login") String customerLogin, @QueryParam("first_name") String firstName, @QueryParam("last_name") String lastName) {
        try {
            L.info(String.format("updateCustomerProfile customerLogin=%s firstName=%s lastName=%s", customerLogin, firstName, lastName));

            UUID customerId = MainFactory.getInstance().getCustomerManager().getCustomerIdByLogin(customerLogin);
            Validate.notNull(customerId);
            Customer customer = MainFactory.getInstance().getCustomerManager().updateCustomer(new Customer().setId(customerId).setFirstName(firstName).setLastName(lastName));

            L.info(String.format("updateCustomProfile customer=%s", customer));

            return Response.ok().entity(JsonMapper.toJson(customer, true)).build();
        } catch (IllegalArgumentException ex) {
            return Response.status(Response.Status.BAD_REQUEST).entity(ex.getMessage() + "\n" + ExceptionUtils.getFullStackTrace(ex)).build();
        }
    }

    @RolesAllowed(AuthenticationFilter.UNKNOWN)
    @POST
    @Path("top_up_balance/{customer_login}")
    public Response topUpBalance(@PathParam("customer_login") String customerLogin, @QueryParam("amount") int amount) {
        try {
            L.info(String.format("topUpBalance customerLogin=%s amount=%s", customerLogin, amount));

            UUID customerId = MainFactory.getInstance().getCustomerManager().getCustomerIdByLogin(customerLogin);
            MainFactory.getInstance().getCustomerManager().topUpBalance(customerId, amount);

            return Response.ok().build();
        } catch (IllegalArgumentException ex) {
            return Response.status(Response.Status.BAD_REQUEST).entity(ex.getMessage() + "\n" + ExceptionUtils.getFullStackTrace(ex)).build();
        }
    }

    @RolesAllowed(AuthenticationFilter.UNKNOWN)
    @GET
    @Path(SUBSCRIPTION_PATH)
    public Response getSubscriptions() {
        try {
            L.info("getSubscriptions");

            List<Subscription> subscriptions = MainFactory.getInstance().getSubscriptionManager().getSubscriptions();

            L.info(String.format("getSubscriptions subscriptions=%s", subscriptions));

            return Response.ok().entity(JsonMapper.toJson(subscriptions, true)).build();
        } catch (IllegalArgumentException ex) {
            return Response.status(Response.Status.BAD_REQUEST).entity(ex.getMessage() + "\n" + ExceptionUtils.getFullStackTrace(ex)).build();
        }
    }

    @RolesAllowed(AuthenticationFilter.UNKNOWN)
    @GET
    @Path("get_customer_subscriptions/{customer_login}")
    public Response getCustomerSubscriptions(@PathParam("customer_login") String customerLogin) {
        try {
            L.info(String.format("getCustomerSubscriptions customerLogin=%s", customerLogin));

            UUID customerId = MainFactory.getInstance().getCustomerManager().getCustomerIdByLogin(customerLogin);
            Validate.notNull(customerId);
            List<Subscription> subscriptions = MainFactory.getInstance().getSubscriptionManager().getSubscriptionsByCID(customerId);

            L.info(String.format("getCustomerSubscriptions subscriptions=%s", subscriptions));

            return Response.ok().entity(JsonMapper.toJson(subscriptions, true)).build();
        } catch (IllegalArgumentException ex) {
            return Response.status(Response.Status.BAD_REQUEST).entity(ex.getMessage() + "\n" + ExceptionUtils.getFullStackTrace(ex)).build();
        }
    }

    @RolesAllowed(AuthenticationFilter.UNKNOWN)
    @POST
    @Path(SUBSCRIPTION_PATH)
    public Response createSubscription(String subscriptionDataJson) {
        try {
            L.info(String.format("createSubscription subscriptionDataJson=%s", subscriptionDataJson));

            Subscription subscriptionData = JsonMapper.fromJson(subscriptionDataJson, Subscription.class);
            Subscription subscription = MainFactory.getInstance().getSubscriptionManager().createSubscription(subscriptionData);

            L.info(String.format("createSubscription subscription=%s", subscription));

            return Response.ok().entity(JsonMapper.toJson(subscription, true)).build();
        } catch (IllegalArgumentException ex) {
            return Response.status(Response.Status.BAD_REQUEST).entity(ex.getMessage() + "\n" + ExceptionUtils.getFullStackTrace(ex)).build();
        }
    }

    @RolesAllowed(AuthenticationFilter.UNKNOWN)
    @DELETE
    @Path(SUBSCRIPTION_PATH)
    public Response deleteSubscription(@QueryParam("customerLogin") String customerLogin, @QueryParam("planName") String planName) {
        try {
            L.info(String.format("deleteSubscription customerLogin=%s planName=%s", customerLogin, planName));

            UUID customerId = MainFactory.getInstance().getCustomerManager().getCustomerIdByLogin(customerLogin);
            Validate.notNull(customerId);
            UUID planId = MainFactory.getInstance().getPlanManager().getPlanIdByName(planName);
            Validate.notNull(planId);
            UUID subscriptionId = MainFactory.getInstance().getSubscriptionManager().getSubscriptionIdByCIDAndPID(customerId, planId);
            Validate.notNull(subscriptionId);
            MainFactory.getInstance().getSubscriptionManager().removeSubscription(subscriptionId);

            return Response.ok().build();
        } catch (IllegalArgumentException ex) {
            return Response.status(Response.Status.BAD_REQUEST).entity(ex.getMessage() + "\n" + ExceptionUtils.getFullStackTrace(ex)).build();
        }
    }

    /* ************************************************************************************************************** */
    /* ********************************ADMIN METHODS***************************************************************** */
    /* ************************************************************************************************************** */

    @RolesAllowed(AuthenticationFilter.ADMIN)
    @GET
    @Path(CUSTOMER_PATH)
    public Response getCustomers() {
        try {
            L.info("getCustomers");

            List<Customer> customers = MainFactory.getInstance().getCustomerManager().getCustomers();

            L.info(String.format("getCustomers customers=%s", customers));

            return Response.ok().entity(JsonMapper.toJson(customers, true)).build();
        } catch (IllegalArgumentException ex) {
            return Response.status(Response.Status.BAD_REQUEST).entity(ex.getMessage() + "\n" + ExceptionUtils.getFullStackTrace(ex)).build();
        }
    }

    @RolesAllowed(AuthenticationFilter.ADMIN)
    @POST
    @Path(CUSTOMER_PATH)
    public Response createCustomer(String customerDataJson) {
        try {
            L.info(String.format("createCustomer customerDataJson=%s", customerDataJson));

            Customer customerData = JsonMapper.fromJson(customerDataJson, Customer.class);
            Customer customer = MainFactory.getInstance().getCustomerManager().createCustomer(customerData);

            L.info(String.format("createCustomer customer=%s", customer));

            return Response.ok().entity(JsonMapper.toJson(customer, true)).build();
        } catch (IllegalArgumentException ex) {
            return Response.status(Response.Status.BAD_REQUEST).entity(ex.getMessage() + "\n" + ExceptionUtils.getFullStackTrace(ex)).build();
        }
    }


    @RolesAllowed(AuthenticationFilter.ADMIN)
    @PUT
    @Path(CUSTOMER_PATH)
    public Response updateCustomer(String customerDataJson) {
        try {
            L.info(String.format("updateCustomer customerDataJson=%s", customerDataJson));

            Customer customerData = JsonMapper.fromJson(customerDataJson, Customer.class);
            UUID customerId = MainFactory.getInstance().getCustomerManager().getCustomerIdByLogin(customerData.getLogin());
            customerData.setId(customerId);
            Customer customer = MainFactory.getInstance().getCustomerManager().updateCustomer(customerData);

            L.info(String.format("updateCustomer customer=%s", customer));

            return Response.ok().entity(JsonMapper.toJson(customer, true)).build();
        } catch (IllegalArgumentException ex) {
            return Response.status(Response.Status.BAD_REQUEST).entity(ex.getMessage() + "\n" + ExceptionUtils.getFullStackTrace(ex)).build();
        }
    }

    @RolesAllowed(AuthenticationFilter.ADMIN)
    @DELETE
    @Path(CUSTOMER_PATH)
    public Response deleteCustomer(@QueryParam("id") String id) {
        try {
            L.info(String.format("deleteCustomer id=%s", id));

            UUID uuid = UUID.fromString(id);
            MainFactory.getInstance().getCustomerManager().removeCustomer(uuid);

            return Response.ok().build();
        } catch (IllegalArgumentException ex) {
            return Response.status(Response.Status.BAD_REQUEST).entity(ex.getMessage() + "\n" + ExceptionUtils.getFullStackTrace(ex)).build();
        }
    }

    @RolesAllowed(AuthenticationFilter.ADMIN)
    @GET
    @Path(PLAN_PATH)
    public Response getPlans() {
        try {
            L.info("getPlans");

            List<Plan> plans = MainFactory.getInstance().getPlanManager().getPlans();

            L.info(String.format("getPlans plans=%s", plans));

            return Response.ok().entity(JsonMapper.toJson(plans, true)).build();
        } catch (IllegalArgumentException ex) {
            return Response.status(Response.Status.BAD_REQUEST).entity(ex.getMessage() + "\n" + ExceptionUtils.getFullStackTrace(ex)).build();
        }
    }

    @RolesAllowed(AuthenticationFilter.ADMIN)
    @POST
    @Path(PLAN_PATH)
    public Response createPlan(String planDataJson) {
        try {
            L.info(String.format("createPlan planDataJson=%s", planDataJson));

            Plan planData = JsonMapper.fromJson(planDataJson, Plan.class);
            Plan plan = MainFactory.getInstance().getPlanManager().createPlan(planData);

            L.info(String.format("createPlan plan=%s", plan));
            // send the answer
            return Response.ok().entity(JsonMapper.toJson(plan, true)).build();
        } catch (IllegalArgumentException ex) {
            return Response.status(Response.Status.BAD_REQUEST).entity(ex.getMessage() + "\n" + ExceptionUtils.getFullStackTrace(ex)).build();
        }
    }


    @RolesAllowed(AuthenticationFilter.ADMIN)
    @PUT
    @Path(PLAN_PATH)
    public Response updatePlan(String planDataJson) {
        try {
            L.info(String.format("updatePlan planDataJson=%s", planDataJson));

            Plan planData = JsonMapper.fromJson(planDataJson, Plan.class);
            UUID planId = MainFactory.getInstance().getPlanManager().getPlanIdByName(planData.getName());
            planData.setId(planId);
            Plan plan = MainFactory.getInstance().getPlanManager().updatePlan(planData);

            L.info(String.format("updatePlan plan=%s", plan));

            return Response.ok().entity(JsonMapper.toJson(plan, true)).build();
        } catch (IllegalArgumentException ex) {
            return Response.status(Response.Status.BAD_REQUEST).entity(ex.getMessage() + "\n" + ExceptionUtils.getFullStackTrace(ex)).build();
        }
    }

    @RolesAllowed(AuthenticationFilter.ADMIN)
    @DELETE
    @Path(PLAN_PATH)
    public Response deletePlan(@QueryParam("id") String id) {
        try {
            L.info(String.format("deletePlan id=%s", id));

            UUID uuid = UUID.fromString(id);
            MainFactory.getInstance().getPlanManager().removePlan(uuid);

            return Response.ok().build();
        } catch (IllegalArgumentException ex) {
            return Response.status(Response.Status.BAD_REQUEST).entity(ex.getMessage() + "\n" + ExceptionUtils.getFullStackTrace(ex)).build();
        }
    }

    @RolesAllowed(AuthenticationFilter.ADMIN)
    @GET
    @Path("/get_customer_id/{customer_login}")
    public Response getCustomerId(@PathParam("customer_login") String customerLogin) {
        try {
            L.info(String.format("getCustomerId customerLogin=%s", customerLogin));

            UUID customerId = MainFactory.getInstance().getCustomerManager().getCustomerIdByLogin(customerLogin);

            L.info(String.format("getCustomerId customerId=%s", customerLogin));

            return Response.ok().entity(JsonMapper.toJson(customerId, true)).build();
        } catch (IllegalArgumentException ex) {
            return Response.status(Response.Status.BAD_REQUEST).entity(ex.getMessage() + "\n" + ExceptionUtils.getFullStackTrace(ex)).build();
        }
    }

    @RolesAllowed(AuthenticationFilter.ADMIN)
    @GET
    @Path("/get_plan_id/{plan_name}")
    public Response getPlanId(@PathParam("plan_name") String planName) {
        try {
            L.info(String.format("getPlanId planName=%s", planName));

            UUID planId = MainFactory.getInstance().getPlanManager().getPlanIdByName(planName);

            L.info(String.format("getPlanId planId=%s", planId));

            return Response.ok().entity(JsonMapper.toJson(planId, true)).build();
        } catch (IllegalArgumentException ex) {
            return Response.status(Response.Status.BAD_REQUEST).entity(ex.getMessage() + "\n" + ExceptionUtils.getFullStackTrace(ex)).build();
        }
    }
}
package ru.nsu.fit.endpoint.service.manager;

import org.apache.commons.lang.Validate;
import org.slf4j.Logger;
import ru.nsu.fit.endpoint.service.database.DBService;
import ru.nsu.fit.endpoint.service.database.data.Customer;

import java.util.List;
import java.util.UUID;
import java.util.regex.Pattern;

public class CustomerManager extends ParentManager {

    private static Pattern firstLastNamePattern = Pattern.compile("^[A-Z][a-z]+");
    private static Pattern emailPattern = Pattern.compile("(^[a-zA-Z0-9_.+-]+@[a-zA-Z0-9-]+\\.[a-zA-Z0-9-.]+(\\.[a-zA-Z0-9-.]+)*)");

    public CustomerManager(DBService dbService, Logger flowLog) {
        super(dbService, flowLog);
    }

    /**
     * Метод создает новый объект типа Customer. Ограничения:
     * Аргумент 'customerData' - не null;
     * firstName - нет пробелов, длина от 2 до 12 символов включительно, начинается с заглавной буквы, остальные символы строчные, нет цифр и других символов;
     * lastName - нет пробелов, длина от 2 до 12 символов включительно, начинается с заглавной буквы, остальные символы строчные, нет цифр и других символов;
     * login - указывается в виде email, проверить email на корректность, проверить что нет customer с таким же email;
     * pass - длина от 6 до 12 символов включительно, не должен быть простым (123qwe или 1q2w3e), не должен содержать части login, firstName, lastName
     * money - должно быть равно 0.
     */
    public Customer createCustomer(Customer customer) {
        Validate.notNull(customer, "Argument 'customerData' is null.");

        validateFirstName(customer);
        validateLastName(customer);

        Validate.notNull(customer.getLogin());
        Validate.isTrue(emailPattern.matcher(customer.getLogin()).matches(), "Email doesn't match pattern");
        Validate.isTrue(!dbService.isCustomerWithLoginExist(customer.getLogin()));

        Validate.notNull(customer.getPass());
        Validate.isTrue(customer.getPass().length() >= 6 && customer.getPass().length() < 13, "Password's length should be more or equal 6 symbols and less or equal 12 symbols.");
        Validate.isTrue(!customer.getPass().equalsIgnoreCase("123qwe"), "Password is easy.");
        Validate.isTrue(!customer.getPass().equalsIgnoreCase("1q2w3e"), "Password is easy.");
        Validate.isTrue(!customer.getPass().contains(customer.getLogin()), "Password contains login");
        Validate.isTrue(!customer.getPass().contains(customer.getFirstName()), "Password contains first name");
        Validate.isTrue(!customer.getPass().contains(customer.getLastName()), "Password contains last name");

        Validate.isTrue(customer.getBalance() == 0, "Balance should be zero");

        return dbService.createCustomer(customer);
    }

    private void validateFirstName(Customer customer) {
        Validate.notNull(customer.getFirstName());
        Validate.isTrue(!customer.getFirstName().contains(" "), "First name shouldn't contains spaces");
        Validate.isTrue(customer.getFirstName().length() >= 2 && customer.getFirstName().length() <= 12, "First name length should be more or equal 2 symbols and less or equal 12 symbols.");
        Validate.isTrue(firstLastNamePattern.matcher(customer.getFirstName()).matches(), "First name should contains only letters, first letter should be uppercase and others lowercase");
    }

    private void validateLastName(Customer customer) {
        Validate.notNull(customer.getLastName());
        Validate.isTrue(!customer.getLastName().contains(" "), "Last name shouldn't contains spaces");
        Validate.isTrue(customer.getLastName().length() >= 2 && customer.getLastName().length() <= 12, "Last name length should be more or equal 2 symbols and less or equal 12 symbols.");
        Validate.isTrue(firstLastNamePattern.matcher(customer.getLastName()).matches(), "Last name should contains only letters, first letter should be uppercase and others lowercase");
    }

    /**
     * Метод возвращает список объектов типа customer.
     */
    public List<Customer> getCustomers() {
        return dbService.getCustomers();
    }


    /**
     * Метод обновляет объект типа Customer.
     * Можно обновить только firstName и lastName.
     */
    public Customer updateCustomer(Customer customer) {
        validateFirstName(customer);
        validateLastName(customer);
        return dbService.updateCustomerData(customer);
    }

    public UUID getCustomerIdByLogin(String login) {
        return dbService.getCustomerIdByLogin(login);
    }

    public void removeCustomer(UUID id) {
        dbService.removeCustomer(id);
    }

    /**
     * Метод добавляет к текущему баласу amount.
     * amount - должен быть строго больше нуля.
     */
    // TODO think about return type of this method
    public void topUpBalance(UUID customerId, int amount) {
        if (amount <= 0) {
            throw new IllegalArgumentException("Amount should me more than 0");
        }
        dbService.updateCustomerBalance(customerId, amount);
    }

}

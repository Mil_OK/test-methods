package ru.nsu.fit.endpoint.service.database.data;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;

import java.util.UUID;

@JsonInclude(JsonInclude.Include.NON_NULL)
public class Subscription {
    @JsonProperty("id")
    private UUID id;

    @JsonProperty("customerId")
    private UUID customerId;

    @JsonProperty("planId")
    private UUID planId;

    public UUID getId() {
        return id;
    }

    public Subscription setId(UUID id) {
        this.id = id;
        return this;
    }

    public UUID getCustomerId() {
        return customerId;
    }

    public Subscription setCustomerId(UUID customerId) {
        this.customerId = customerId;
        return this;
    }

    public UUID getPlanId() {
        return planId;
    }

    public Subscription setPlanId(UUID planId) {
        this.planId = planId;
        return this;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        Subscription that = (Subscription) o;

        return id != null ? id.equals(that.id) : that.id == null;
    }

    @Override
    public int hashCode() {
        return id != null ? id.hashCode() : 0;
    }

    @Override
    public Subscription clone() {
        return new Subscription()
                .setId(id)
                .setCustomerId(customerId)
                .setPlanId(planId);
    }

    @Override
    public String toString() {
        return "Subscription{" +
                "id=" + id +
                ", customerId=" + customerId +
                ", planId=" + planId +
                '}';
    }
}

package ru.nsu.fit.endpoint.service.manager;

import org.apache.commons.lang.NotImplementedException;
import org.apache.commons.lang.Validate;
import org.slf4j.Logger;
import ru.nsu.fit.endpoint.service.database.DBService;
import ru.nsu.fit.endpoint.service.database.data.Customer;
import ru.nsu.fit.endpoint.service.database.data.Plan;
import ru.nsu.fit.endpoint.service.database.data.Subscription;

import java.util.List;
import java.util.UUID;

public class SubscriptionManager extends ParentManager {
    public SubscriptionManager(DBService dbService, Logger flowLog) {
        super(dbService, flowLog);
    }

    /**
     * Метод создает подписку. Ограничения:
     * 1. Подписки с таким планом пользователь не имеет.
     * 2. Стоймость подписки не превышает текущего баланса кастомера и после покупки вычитается из его баласа.
     */
    // TODO should we check is exist customer and plan with this ids
    public Subscription createSubscription(Subscription subscription) {
        Validate.notNull(subscription);
        Validate.notNull(subscription.getCustomerId());
        Validate.notNull(subscription.getPlanId());
        Validate.isTrue(!dbService.isSubscriptionWithCustomerIdAndPlanIdExist(subscription.getCustomerId(), subscription.getPlanId()));
        Customer customer = dbService.getCustomerById(subscription.getCustomerId());
        Plan plan = dbService.getPlanById(subscription.getPlanId());
        Validate.isTrue(customer.getBalance() >= plan.getFee());
        Subscription s = dbService.createSubscription(subscription);
        dbService.updateCustomerBalance(s.getCustomerId(), -1*plan.getFee());
        return s;
    }

    public void removeSubscription(UUID subscriptionId) {
        dbService.removeSubscription(subscriptionId);
    }

    public UUID getSubscriptionIdByCIDAndPID(UUID customerId, UUID planId) {
        return dbService.getSubscriptionByCustomerIdAndPlanId(customerId, planId);
    }



    /**
     * Возвращает список подписок указанного customer'а.
     */
    public List<Subscription> getSubscriptionsByCID(UUID customerId) {
        return dbService.getSubscriptions();
    }


    public List<Subscription> getSubscriptions() {
        return dbService.getSubscriptions();
    }
}

package ru.nsu.fit.endpoint.service.manager;

import org.apache.commons.lang.Validate;
import org.slf4j.Logger;
import ru.nsu.fit.endpoint.service.database.DBService;
import ru.nsu.fit.endpoint.service.database.data.Plan;

import java.util.List;
import java.util.UUID;
import java.util.regex.Pattern;

public class PlanManager extends ParentManager {
    public PlanManager(DBService dbService, Logger flowLog) {
        super(dbService, flowLog);
    }

    private static Pattern namePattern = Pattern.compile("^[A-Za-z]+");

    /**
     * Метод создает новый объект типа Plan. Ограничения:
     * name - длина не больше 128 символов и не меньше 2 включительно не содержит спец символов. Имена не пересекаются друг с другом;
    /* details - длина не больше 1024 символов и не меньше 1 включительно;
    /* fee - больше либо равно 0 но меньше либо равно 999999.
     */
    public Plan createPlan(Plan plan) {
        Validate.notNull(plan, "Argument 'planData' is null.");
        validateName(plan);
        validateDetails(plan);
        validateFee(plan);
        return dbService.createPlan(plan);
    }

    public Plan updatePlan(Plan plan) {
        validateDetails(plan);
        validateFee(plan);
        return dbService.updatePlan(plan);
    }

    public void removePlan(UUID id) {
        dbService.removePlan(id);
    }

    /**
     * Метод возвращает список планов доступных для покупки.
     */
    public List<Plan> getPlans() {
        return dbService.getPlans();
    }

    public UUID getPlanIdByName(String name) {
        return dbService.getPlanIdByName(name);
    }

    private void validateName(Plan plan) {
        Validate.notNull(plan.getName());
        Validate.isTrue(plan.getName().length() >= 2 && plan.getName().length() <= 128, "Plan name length must be from 2 to 128");
        Validate.isTrue(namePattern.matcher(plan.getName()).matches(), "Plan name must contains only letters");
        Validate.isTrue(!dbService.isPlanWithNameExist(plan.getName()));
    }

    private void validateDetails(Plan plan) {
        Validate.notNull(plan.getDetails());
        Validate.isTrue(plan.getDetails().length() >= 1 && plan.getDetails().length() <= 1024, "Plan details length must be from 1 to 1024");
    }

    private void validateFee(Plan plan) {
        Validate.isTrue(plan.getFee() >= 0 && plan.getFee() <= 999999, "Plan fee must be from 0 to 999999");
    }
}

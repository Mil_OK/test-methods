package ru.nsu.fit.endpoint.service.manager;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.mockito.Mockito;
import org.slf4j.Logger;
import ru.nsu.fit.endpoint.service.database.DBService;
import ru.nsu.fit.endpoint.service.database.data.Customer;

import java.util.UUID;

public class CustomerManagerTest {
    private DBService dbService;
    private Logger logger;
    private CustomerManager customerManager;

    private Customer customerBeforeCreateMethod;
    private Customer customerAfterCreateMethod;

    @Before
    public void before() {
        // create stubs for the test's class
        dbService = Mockito.mock(DBService.class);
        logger = Mockito.mock(Logger.class);

        customerBeforeCreateMethod = new Customer()
                .setId(null)
                .setFirstName("John")
                .setLastName("Wick")
                .setLogin("john_wick@gmail.com")
                .setPass("Baba_Jaga")
                .setBalance(0);
        customerAfterCreateMethod = customerBeforeCreateMethod.clone();
        customerAfterCreateMethod.setId(UUID.randomUUID());

        Mockito.when(dbService.createCustomer(customerBeforeCreateMethod)).thenReturn(customerAfterCreateMethod);

        // create the test's class
        customerManager = new CustomerManager(dbService, logger);
    }

    @Test
    public void testCreateNewCustomer() {
        // Вызываем метод, который хотим протестировать
        Customer customer = customerManager.createCustomer(customerBeforeCreateMethod);

        // Проверяем результат выполенния метода
        Assert.assertEquals(customer.getId(), customerAfterCreateMethod.getId());

        // Проверяем, что метод мока базы данных был вызван 1 раз
        Assert.assertEquals(1, Mockito.mockingDetails(dbService).getInvocations().stream()
                .filter(i -> i.getMethod().getName().equals("createCustomer"))
                .count());
    }

    @Test
    public void testCreateCustomerWithNullArgument() {
        try {
            customerManager.createCustomer(null);
            Assert.fail();
        } catch (IllegalArgumentException ex) {
            Assert.assertEquals("Argument 'customerData' is null.", ex.getMessage());
        }
    }

    @Test
    public void testCreateCustomerWithEasyPassword() {
        try {
            customerBeforeCreateMethod.setPass("123qwe");
            customerManager.createCustomer(customerBeforeCreateMethod);
            Assert.fail();
        } catch (IllegalArgumentException ex) {
            Assert.assertEquals("Password is easy.", ex.getMessage());
        }
    }

    @Test
    public void testCreateCustomerWithShortFirstName() {
        try {
            customerBeforeCreateMethod.setFirstName("J");
            customerManager.createCustomer(customerBeforeCreateMethod);
            Assert.fail();
        } catch (IllegalArgumentException ex) {
            Assert.assertEquals("First name length should be more or equal 2 symbols and less or equal 12 symbols.", ex.getMessage());
        }
    }

    @Test
    public void testCreateCustomerWithLongFirstName() {
        try {
            customerBeforeCreateMethod.setFirstName("JoooooohnyBoy");
            customerManager.createCustomer(customerBeforeCreateMethod);
            Assert.fail();
        } catch (IllegalArgumentException ex) {
            Assert.assertEquals("First name length should be more or equal 2 symbols and less or equal 12 symbols.", ex.getMessage());
        }
    }

    @Test
    public void testCreateCustomerWithNonUpperCaseFirstLetterFirstName() {
        try {
            customerBeforeCreateMethod.setFirstName("john");
            customerManager.createCustomer(customerBeforeCreateMethod);
            Assert.fail();
        } catch (IllegalArgumentException ex) {
            Assert.assertEquals("First name should contains only letters, first letter should be uppercase and others lowercase", ex.getMessage());
        }
    }

    @Test
    public void testCreateCustomerWithShortLastName() {
        try {
            customerBeforeCreateMethod.setLastName("W");
            customerManager.createCustomer(customerBeforeCreateMethod);
            Assert.fail();
        } catch (IllegalArgumentException ex) {
            Assert.assertEquals("Last name length should be more or equal 2 symbols and less or equal 12 symbols.", ex.getMessage());
        }
    }

    @Test
    public void testCreateCustomerWithLongLastName() {
        try {
            customerBeforeCreateMethod.setLastName("MilkyyyyyyWay");
            customerManager.createCustomer(customerBeforeCreateMethod);
            Assert.fail();
        } catch (IllegalArgumentException ex) {
            Assert.assertEquals("Last name length should be more or equal 2 symbols and less or equal 12 symbols.", ex.getMessage());
        }
    }

    @Test
    public void testCreateCustomerWithNonUpperCaseFirstLetterLastName() {
        try {
            customerBeforeCreateMethod.setLastName("wick");
            customerManager.createCustomer(customerBeforeCreateMethod);
            Assert.fail();
        } catch (IllegalArgumentException ex) {
            Assert.assertEquals("Last name should contains only letters, first letter should be uppercase and others lowercase", ex.getMessage());
        }
    }

    @Test
    public void testCreateCustomerWithIncorrectEmailInLogin() {
        try {
            customerBeforeCreateMethod.setLogin("john@wick");
            customerManager.createCustomer(customerBeforeCreateMethod);
            Assert.fail();
        } catch (IllegalArgumentException ex) {
            Assert.assertEquals("Email doesn't match pattern", ex.getMessage());
        }
    }

    @Test
    public void testCreateCustomerWithContainedLastNamePass() {
        try {
            customerBeforeCreateMethod.setPass("WickPass");
            customerManager.createCustomer(customerBeforeCreateMethod);
            Assert.fail();
        } catch (IllegalArgumentException ex) {
            Assert.assertEquals("Password contains last name", ex.getMessage());
        }
    }


    @Test
    public void testCreateCustomerWithNonZeroBalance() {
        try {
            customerBeforeCreateMethod.setBalance(1000);
            customerManager.createCustomer(customerBeforeCreateMethod);
            Assert.fail();
        } catch (IllegalArgumentException ex) {
            Assert.assertEquals("Balance should be zero", ex.getMessage());
        }
    }
}

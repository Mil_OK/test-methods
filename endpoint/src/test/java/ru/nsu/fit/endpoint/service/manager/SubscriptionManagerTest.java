package ru.nsu.fit.endpoint.service.manager;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.mockito.Mockito;
import org.slf4j.Logger;
import ru.nsu.fit.endpoint.service.database.DBService;
import ru.nsu.fit.endpoint.service.database.data.Customer;
import ru.nsu.fit.endpoint.service.database.data.Plan;
import ru.nsu.fit.endpoint.service.database.data.Subscription;

import java.util.UUID;

public class SubscriptionManagerTest {
    private DBService dbService;
    private Logger logger;
    private SubscriptionManager subscriptionManager;

    private Customer customer;
    private Plan plan;
    private Subscription subscriptionBeforeCreateMethod;
    private Subscription subscriptionAfterCreateMethod;

    @Before
    public void before() {
        // create stubs for the test's class
        dbService = Mockito.mock(DBService.class);
        logger = Mockito.mock(Logger.class);

        customer = new Customer()
                .setId(UUID.randomUUID())
                .setFirstName("John")
                .setLastName("Wick")
                .setLogin("john_wick@gmail.com")
                .setPass("Baba_Jaga")
                .setBalance(1000);
        plan = new Plan()
                .setId(UUID.randomUUID())
                .setName("planName")
                .setDetails("Plan details")
                .setFee(100);
        subscriptionBeforeCreateMethod = new Subscription()
                .setId(null)
                .setCustomerId(customer.getId())
                .setPlanId(plan.getId());
        subscriptionAfterCreateMethod = subscriptionBeforeCreateMethod.clone();
        subscriptionAfterCreateMethod.setId(UUID.randomUUID());

        Mockito.when(dbService.createSubscription(subscriptionBeforeCreateMethod)).thenReturn(subscriptionAfterCreateMethod);
        Mockito.when(dbService.getCustomerById(customer.getId())).thenReturn(customer);
        Mockito.when(dbService.getPlanById(plan.getId())).thenReturn(plan);
        Mockito.doAnswer(invocationOnMock -> {
            customer.setBalance(customer.getBalance() + (int) invocationOnMock.getArguments()[1]);
            return null;
        }).when(dbService).updateCustomerBalance(customer.getId(), -1 * plan.getFee());

        // create the test's class
        CustomerManager customerManager = new CustomerManager(dbService, logger);
        PlanManager planManager = new PlanManager(dbService, logger);
        subscriptionManager = new SubscriptionManager(dbService, logger);
    }

    @Test
    public void testCreateNewSubscription() {
        // Вызываем метод, который хотим протестировать
        Subscription subscription = subscriptionManager.createSubscription(subscriptionBeforeCreateMethod);

        // Проверяем результат выполенния метода
        Assert.assertEquals(subscription.getId(), subscriptionAfterCreateMethod.getId());

        // Проверяем, что метод мока базы данных был вызван 1 раз
        Assert.assertEquals(1, Mockito.mockingDetails(dbService).getInvocations().stream()
                .filter(i -> i.getMethod().getName().equals("createSubscription"))
                .count());
        Assert.assertEquals(1, Mockito.mockingDetails(dbService).getInvocations().stream()
                .filter(i -> i.getMethod().getName().equals("updateCustomerBalance"))
                .count());
        Assert.assertEquals(900, customer.getBalance());
    }
}

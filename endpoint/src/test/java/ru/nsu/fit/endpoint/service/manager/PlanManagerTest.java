package ru.nsu.fit.endpoint.service.manager;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.mockito.Mockito;
import org.slf4j.Logger;
import ru.nsu.fit.endpoint.service.database.DBService;
import ru.nsu.fit.endpoint.service.database.data.Plan;

import java.util.UUID;

public class PlanManagerTest {
    private DBService dbService;
    private Logger logger;
    private PlanManager planManager;

    private Plan planBeforeCreateMethod;
    private Plan planAfterCreateMethod;

    @Before
    public void before() {
        // create stubs for the test's class
        dbService = Mockito.mock(DBService.class);
        logger = Mockito.mock(Logger.class);

        planBeforeCreateMethod = new Plan()
                .setId(null)
                .setName("planName")
                .setDetails("Plan details")
                .setFee(100);
        planAfterCreateMethod = planBeforeCreateMethod.clone();
        planAfterCreateMethod.setId(UUID.randomUUID());

        Mockito.when(dbService.createPlan(planBeforeCreateMethod)).thenReturn(planAfterCreateMethod);

        // create the test's class
        planManager = new PlanManager(dbService, logger);
    }

    @Test
    public void testCreateNewPlan() {
        // Вызываем метод, который хотим протестировать
        Plan plan = planManager.createPlan(planBeforeCreateMethod);

        // Проверяем результат выполенния метода
        Assert.assertEquals(plan.getId(), planAfterCreateMethod.getId());

        // Проверяем, что метод мока базы данных был вызван 1 раз
        Assert.assertEquals(1, Mockito.mockingDetails(dbService).getInvocations().stream()
                .filter(i -> i.getMethod().getName().equals("createPlan"))
                .count());
    }

    @Test
    public void testRemovePlan() {
        // Вызываем метод, который хотим протестировать
        Plan plan = planManager.createPlan(planBeforeCreateMethod);

        // Проверяем результат выполенния метода
        Assert.assertEquals(plan.getId(), planAfterCreateMethod.getId());

        // Проверяем, что метод мока базы данных был вызван 1 раз
        Assert.assertEquals(1, Mockito.mockingDetails(dbService).getInvocations().stream()
                .filter(i -> i.getMethod().getName().equals("createPlan"))
                .count());
    }

    @Test
    public void testUpdatePlan() {
        Plan plan = planManager.createPlan(planBeforeCreateMethod);
        Assert.assertEquals(plan.getId(), planAfterCreateMethod.getId());

        // Проверяем, что метод мока базы данных был вызван 1 раз
        Assert.assertEquals(1, Mockito.mockingDetails(dbService).getInvocations().stream()
                .filter(i -> i.getMethod().getName().equals("createPlan"))
                .count());
    }

    @Test
    public void testCreatePlanWithNullArgument() {
        try {
            planManager.createPlan(null);
        } catch (IllegalArgumentException ex) {
            Assert.assertEquals("Argument 'planData' is null.", ex.getMessage());
        }
    }

    @Test
    public void testCreatePlanWithShortName() {
        try {
            planBeforeCreateMethod.setName("p");
            planManager.createPlan(planBeforeCreateMethod);
            Assert.fail();
        } catch (IllegalArgumentException ex) {
            Assert.assertEquals("Plan name length must be from 2 to 128", ex.getMessage());
        }
    }

    @Test
    public void testCreatePlanWithLongName() {
        try {
            StringBuilder longPlanName = new StringBuilder("");
            for (int i = 0; i < 42; i++) {
                longPlanName.append("plan");
            }
            planBeforeCreateMethod.setName(longPlanName.toString());
            planManager.createPlan(planBeforeCreateMethod);
            Assert.fail();
        } catch (IllegalArgumentException ex) {
            Assert.assertEquals("Plan name length must be from 2 to 128", ex.getMessage());
        }
    }

    @Test
    public void testCreatePlanWithSpecificCharactersName() {
        try {
            planBeforeCreateMethod.setName("plan-name #1");
            planManager.createPlan(planBeforeCreateMethod);
            Assert.fail();
        } catch (IllegalArgumentException ex) {
            Assert.assertEquals("Plan name must contains only letters", ex.getMessage());
        }
    }

    @Test
    public void testCreatePlanWithEmptyDetails() {
        try {
            planBeforeCreateMethod.setDetails("");
            planManager.createPlan(planBeforeCreateMethod);
            Assert.fail();
        } catch (IllegalArgumentException ex) {
            Assert.assertEquals("Plan details length must be from 1 to 1024", ex.getMessage());
        }
    }

    @Test
    public void testCreatePlanWithLongDetails() {
        try {
            StringBuilder longPlanDetails = new StringBuilder("");
            for (int i = 0; i < 4242; i++) {
                longPlanDetails.append("details");
            }
            planBeforeCreateMethod.setDetails(longPlanDetails.toString());
            planManager.createPlan(planBeforeCreateMethod);
            Assert.fail();
        } catch (IllegalArgumentException ex) {
            Assert.assertEquals("Plan details length must be from 1 to 1024", ex.getMessage());
        }
    }

    @Test
    public void testCreatePlanWithNegativeFee() {
        try {
            planBeforeCreateMethod.setFee(-1);
            planManager.createPlan(planBeforeCreateMethod);
            Assert.fail();
        } catch (IllegalArgumentException ex) {
            Assert.assertEquals("Plan fee must be from 0 to 999999", ex.getMessage());
        }
    }

    @Test
    public void testCreatePlanWithBigFee() {
        try {
            planBeforeCreateMethod.setFee(1_000_000);
            planManager.createPlan(planBeforeCreateMethod);
            Assert.fail();
        } catch (IllegalArgumentException ex) {
            Assert.assertEquals("Plan fee must be from 0 to 999999", ex.getMessage());
        }
    }
}

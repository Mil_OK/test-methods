package ru.nsu.fit.tests;

import com.google.common.base.Joiner;
import org.junit.Assert;
import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.testng.annotations.AfterClass;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;
import ru.nsu.fit.services.browser.Browser;
import ru.nsu.fit.services.browser.BrowserService;
import ru.yandex.qatools.allure.annotations.Description;
import ru.yandex.qatools.allure.annotations.Features;
import ru.yandex.qatools.allure.annotations.Severity;
import ru.yandex.qatools.allure.annotations.Title;
import ru.yandex.qatools.allure.model.SeverityLevel;

import java.util.List;
import java.util.UUID;
import java.util.stream.Collectors;

/**
 * @author Timur Zolotuhin (tzolotuhin@gmail.com)
 */
public class AcceptanceTest {
    private Browser browser = null;

    @BeforeClass
    public void beforeClass() {
        browser = BrowserService.openNewBrowser();
    }

    @Test
    @Title("Create customer")
    @Description("Create customer via UI API")
    @Severity(SeverityLevel.BLOCKER)
    @Features("Customer feature")
    public void createCustomer() {
        Browser browser = BrowserService.openNewBrowser();

        // login to admin cp
        browser.openPage("http://localhost:8080/endpoint");
        browser.waitForElement(By.id("email"));

        browser.getElement(By.id("email")).sendKeys("admin");
        browser.getElement(By.id("password")).sendKeys("setup");

        browser.getElement(By.id("login")).click();

        // create customer
        browser.getElement(By.id("add_new_customer")).click();

        browser.getElement(By.id("first_name_id")).sendKeys("John");
        browser.getElement(By.id("last_name_id")).sendKeys("Weak");
        browser.getElement(By.id("email_id")).sendKeys("email@example.com");
        browser.getElement(By.id("password_id")).sendKeys("strongpass");

        browser.getElement(By.id("create_customer_id")).click();
    }

    @Test(dependsOnMethods = "createCustomer")
    @Title("Check login")
    @Description("Get customer id by login")
    @Severity(SeverityLevel.CRITICAL)
    @Features("Customer feature")
    public void checkCustomer() {
        Browser browser = BrowserService.openNewBrowser();

        browser.openPage("http://localhost:8080/endpoint");
        browser.waitForElement(By.id("email"));

        browser.getElement(By.id("email")).sendKeys("admin");
        browser.getElement(By.id("password")).sendKeys("setup");

        browser.getElement(By.id("login")).click();

        List<WebElement> tableElems = browser.getElement(By.id("customer_list_id")).findElements(By.xpath("id('customer_list_id')/tbody/tr"));
        Assert.assertEquals(1, tableElems.size());

        List<WebElement> row = tableElems.get(0).findElements(By.xpath("td"));
        String rowText = row.stream().map(WebElement::getText).collect(Collectors.joining(" "));
        Assert.assertTrue(rowText.contains("John"));
        Assert.assertTrue(rowText.contains("Weak"));
        Assert.assertTrue(rowText.contains("email@example.com"));
        Assert.assertTrue(rowText.contains("strongpass"));
    }

    @Test
    @Title("Unknown customer credentials")
    @Description("Check error after bad login via UI API")
    @Severity(SeverityLevel.BLOCKER)
    @Features("Customer feature")
    public void unknownCustomerCredentials() {
        Browser browser = BrowserService.openNewBrowser();

        // login to admin cp
        browser.openPage("http://localhost:8080/endpoint");
        browser.waitForElement(By.id("email"));

        browser.getElement(By.id("email")).sendKeys("UNKNOWN_LOGIN" + UUID.randomUUID().toString());
        browser.getElement(By.id("password")).sendKeys("UNKNOWN_PASSWORD" + UUID.randomUUID().toString());

        browser.getElement(By.id("login")).click();

        Assert.assertEquals("Email or password is incorrect", browser.getWebDriver().switchTo().alert().getText());
    }

    @AfterClass
    public void afterClass() {
        if (browser != null)
            browser.close();
    }
}

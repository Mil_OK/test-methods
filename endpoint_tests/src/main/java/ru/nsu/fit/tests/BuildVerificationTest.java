package ru.nsu.fit.tests;

import org.glassfish.jersey.client.ClientConfig;
import org.glassfish.jersey.client.authentication.HttpAuthenticationFeature;
import org.glassfish.jersey.jackson.JacksonFeature;
import org.testng.annotations.Test;
import ru.nsu.fit.shared.AllureUtils;
import ru.yandex.qatools.allure.annotations.*;
import ru.yandex.qatools.allure.model.SeverityLevel;

import javax.ws.rs.client.*;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

/**
 * @author Timur Zolotuhin (tzolotuhin@gmail.com)
 */
@Title("BuildVerificationTest")
public class BuildVerificationTest {
    @Test
    @Title("Create customer")
    @Description("Create customer via REST API")
    @Severity(SeverityLevel.BLOCKER)
    @Features("Customer feature")
    public void createCustomer() {
        ClientConfig clientConfig = new ClientConfig();

        HttpAuthenticationFeature feature = HttpAuthenticationFeature.basic("admin", "setup");
        clientConfig.register( feature) ;

        clientConfig.register(JacksonFeature.class);

        Client client = ClientBuilder.newClient( clientConfig );

        WebTarget webTarget = client.target("http://localhost:8080/endpoint/rest").path("customer");

        Invocation.Builder invocationBuilder =	webTarget.request(MediaType.APPLICATION_JSON);
        Response response = invocationBuilder.post(Entity.entity("{\n" +
                "\t\"firstName\":\"Johnds\",\n" +
                "    \"lastName\":\"Weak\",\n" +
                "    \"login\":\"helloworld123@login.com\",\n" +
                "    \"pass\":\"password123\",\n" +
                "    \"money\":\"100\"\n" +
                "}", MediaType.APPLICATION_JSON));
        AllureUtils.saveTextLog("Response: " + response.readEntity(String.class));
    }

    @Test(dependsOnMethods = "createCustomer")
    @Title("Check login")
    @Description("Get customer id by login")
    @Severity(SeverityLevel.CRITICAL)
    @Features("Customer feature")
    public void checkCustomerCreation() {
        ClientConfig clientConfig = new ClientConfig();

        HttpAuthenticationFeature feature = HttpAuthenticationFeature.basic("admin", "setup");
        clientConfig.register( feature) ;

        clientConfig.register(JacksonFeature.class);

        Client client = ClientBuilder.newClient( clientConfig );

        WebTarget webTarget = client.target("http://localhost:8080/endpoint/rest")
                .path("get_customer_id/helloworld123@login.com");

        Invocation.Builder invocationBuilder =	webTarget.request(MediaType.APPLICATION_JSON);
        Response response = invocationBuilder.get();
        AllureUtils.saveTextLog("Response: " + response.readEntity(String.class));
    }

    @Test
    @Title("Create plan")
    @Description("Create plan via REST API")
    @Severity(SeverityLevel.BLOCKER)
    @Features("Plan feature")
    public void createPlan() {
        ClientConfig clientConfig = new ClientConfig();

        HttpAuthenticationFeature feature = HttpAuthenticationFeature.basic("admin", "setup");
        clientConfig.register( feature) ;

        clientConfig.register(JacksonFeature.class);

        Client client = ClientBuilder.newClient( clientConfig );

        WebTarget webTarget = client.target("http://localhost:8080/endpoint/rest").path("plan");

        Invocation.Builder invocationBuilder =	webTarget.request(MediaType.APPLICATION_JSON);
        Response response = invocationBuilder.post(Entity.entity("{\n" +
                "\t\"name\":\"supa_plan\",\n" +
                "    \"details\":\"bestplan_in_your_life\",\n" +
                "    \"fee\":\"15\",\n" +
                "}", MediaType.APPLICATION_JSON));
        AllureUtils.saveTextLog("Response: " + response.readEntity(String.class));
    }

    @Test(dependsOnMethods = "createPlan")
    @Title("Check login")
    @Description("Get customer id by login")
    @Severity(SeverityLevel.CRITICAL)
    @Features("Plan feature")
    public void checkPlanCreation() {
        ClientConfig clientConfig = new ClientConfig();

        HttpAuthenticationFeature feature = HttpAuthenticationFeature.basic("admin", "setup");
        clientConfig.register( feature) ;

        clientConfig.register(JacksonFeature.class);

        Client client = ClientBuilder.newClient( clientConfig );

        WebTarget webTarget = client.target("http://localhost:8080/endpoint/rest")
                .path("get_plan_id/supa_plan");

        Invocation.Builder invocationBuilder =	webTarget.request(MediaType.APPLICATION_JSON);
        Response response = invocationBuilder.get();
        AllureUtils.saveTextLog("Response: " + response.readEntity(String.class));
    }

    @Test(dependsOnMethods = "createPlan")
    @Title("Check login")
    @Description("delete plan by name")
    @Severity(SeverityLevel.CRITICAL)
    @Features("Plan feature")
    public void deletePlan() {
        ClientConfig clientConfig = new ClientConfig();

        HttpAuthenticationFeature feature = HttpAuthenticationFeature.basic("admin", "setup");
        clientConfig.register( feature) ;

        clientConfig.register(JacksonFeature.class);

        Client client = ClientBuilder.newClient( clientConfig );

        WebTarget webTarget = client.target("http://localhost:8080/endpoint/rest")
                .path("plan").queryParam("id", "supa_plan");

        Invocation.Builder invocationBuilder =	webTarget.request(MediaType.APPLICATION_JSON);
        Response response = invocationBuilder.delete();
        AllureUtils.saveTextLog("Response: " + response.readEntity(String.class));
    }

    @Test(dependsOnMethods = "createPlan")
    @Title("Get all plans")
    @Description("Get plans list")
    @Severity(SeverityLevel.CRITICAL)
    @Features("Plan feature")
    public void plansList() {
        ClientConfig clientConfig = new ClientConfig();

        HttpAuthenticationFeature feature = HttpAuthenticationFeature.basic("admin", "setup");
        clientConfig.register( feature) ;

        clientConfig.register(JacksonFeature.class);

        Client client = ClientBuilder.newClient( clientConfig );

        WebTarget webTarget = client.target("http://localhost:8080/endpoint/rest")
                .path("plan");

        Invocation.Builder invocationBuilder =	webTarget.request();
        Response response = invocationBuilder.get();
        AllureUtils.saveTextLog("Response: " + response.readEntity(String.class));
    }

}
